import * as React from 'react';
// import './App.css';
import List from './ScrollablePane.DetailsList.Example';
// import ExampleCard from './ExampleCard/ExampleCard';
// import { Sticky } from 'office-ui-fabric-react/lib/Sticky';

const logo = require('./logo.svg');

class App extends React.Component {
  render() {
    return (
      <div className="App">
      <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="ExampleCard">
        <div className="ExampleCard-header">
        <span>DetailsList Locked Header</span>
        </div>
        <List/>
        </div>
      </div>
    );
  }
}

export default App;
