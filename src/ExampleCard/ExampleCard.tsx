import * as React from 'react';
import './ExampleCard.scss';

export interface IExampleCardProps {
  title: string;
  isOptIn?: boolean;
  code?: string;
  children?: React.ReactNode;
  isRightAligned?: boolean;
  dos?: JSX.Element;
  donts?: JSX.Element;
}

export interface IExampleCardState {
  isCodeVisible?: boolean;
}

export default class ExampleCard extends React.Component {
    render() {
        return (
            <div className="root">
              <div className="ExampleCard-header">
                <div className="ExampleCard-toggleButtons ms-font-l"/>
              </div>
              <div className="ExampleCard-code"/>
            </div>
          );
    }
}