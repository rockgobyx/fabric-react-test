import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './styles/css/index.css';
import './styles/css/ScrollablePane.Example.css';

ReactDOM.render(
  <App />,
  document.getElementById('content') as HTMLElement
);
registerServiceWorker();
